import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { CursoService } from '../curso.service';

@Component({
  selector: 'app-cursos-listagem',
  templateUrl: './cursos-listagem.component.html',
  styleUrls: ['./cursos-listagem.component.scss']
})
export class CursosListagemComponent implements OnInit {

  cursos: any[] = [];
  form: FormGroup;

  constructor(
    private cursoService: CursoService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.listar();
    this.form = this.formBuilder.group({
      search: ''
    });

  }

  listar() {
    this.cursoService.listar().subscribe(res => {
      this.cursos = res;
    }, err => {
      console.log('err', err);
    });
  }


  listarAZ() {
    this.cursoService.listar().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.titulo > n2.titulo) {
          return 1;
        }

        if (n1.titulo < n2.titulo) {
          return -1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }

  listarZA() {
    this.cursoService.listar().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.titulo > n2.titulo) {
          return -1;
        }

        if (n1.titulo < n2.titulo) {
          return 1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }

  get filtredCurso() {
    const result = this.cursos.filter(
      curso => curso.titulo.toLowerCase().includes(
        this.form.get('search').value.toLowerCase()
      ));
    return result;
  }

  listarAZprof(){
    this.cursoService.listar().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.prof > n2.prof) {
          return 1;
        }

        if (n1.prof < n2.prof) {
          return -1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }
  listarZAprof() {
    this.cursoService.listar().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.prof > n2.prof) {
          return -1;
        }

        if (n1.prof < n2.prof) {
          return 1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }



}