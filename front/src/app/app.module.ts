import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CursosListagemComponent } from './cursos-listagem/cursos-listagem.component';
import { CursoService } from './curso.service';
import { MusicaComponent } from './pages/musica/musica.component';
import { NegociosComponent } from './pages/negocios/negocios.component';
import { DesignComponent } from './pages/design/design.component';
import { DesenvolvimentoComponent } from './pages/desenvolvimento/desenvolvimento.component';
import { FinancasComponent } from './pages/financas/financas.component';
import { EstiloComponent } from './pages/estilo/estilo.component';
import { MarketingComponent } from './pages/marketing/marketing.component';
import { SaudeComponent } from './pages/saude/saude.component';
import { TiComponent } from './pages/ti/ti.component';
import { ProdutividadeComponent } from './pages/produtividade/produtividade.component';
import { PessoalComponent } from './pages/pessoal/pessoal.component';
import { FotografiaComponent } from './pages/fotografia/fotografia.component';
import { EnsinoComponent } from './pages/ensino/ensino.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InicialComponent } from './pages/inicial/inicial.component';



@NgModule({
  declarations: [
    AppComponent,
    CursosListagemComponent,
    MusicaComponent,
    NegociosComponent,
    DesignComponent,
    DesenvolvimentoComponent,
    FinancasComponent,
    EstiloComponent,
    MarketingComponent,
    SaudeComponent,
    TiComponent,
    ProdutividadeComponent,
    PessoalComponent,
    FotografiaComponent,
    EnsinoComponent,
    InicialComponent,
     
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    
  ],
  providers: [ CursoService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
