import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  cursosUrl = 'http://0.0.0.0:3000'
  UrlNegocios = 'http://0.0.0.0:3000/negocios'
  UrlDesign = 'http://0.0.0.0:3000/design'
  UrlDesenvolvimento = 'http://0.0.0.0:3000/desenvolvimento'
  UrlFinancas = 'http://0.0.0.0:3000/financas'
  UrlEstilo = 'http://0.0.0.0:3000/estilo'
  UrlMarketing = 'http://0.0.0.0:3000/marketing'
  UrlMusica = 'http://0.0.0.0:3000/musica'
  UrlSaude = 'http://0.0.0.0:3000/saude'
  UrlTi = 'http://0.0.0.0:3000/ti'
  UrlProdutividade = 'http://0.0.0.0:3000/produtividade'
  UrlPessoal = 'http://0.0.0.0:3000/pessoal'
  UrlFotografia = 'http://0.0.0.0:3000/fotografia'
  UrlEnsino = 'http://0.0.0.0:3000/ensino'

  constructor(private http: HttpClient) { }

  listar(){
    return this.http.get<any[]>(`${this.cursosUrl}`);
  }
  
  listarnegocios(){
    return this.http.get<any[]>(`${this.UrlNegocios}`);
  }
  listardesign(){
    return this.http.get<any[]>(`${this.UrlDesign}`);
  }
  listardesenvolvimento(){
    return this.http.get<any[]>(`${this.UrlDesenvolvimento}`);
  }
  listarfinancas(){
    return this.http.get<any[]>(`${this.UrlFinancas}`);
  }
  listarestilo(){
    return this.http.get<any[]>(`${this.UrlEstilo}`);
  }
  listarmarketing(){
    return this.http.get<any[]>(`${this.UrlMarketing}`);
  }
  listarmusica(){
    return this.http.get<any[]>(`${this.UrlMusica}`);
  }
  listarsaude(){
    return this.http.get<any[]>(`${this.UrlSaude}`);
  }
  listarti(){
    return this.http.get<any[]>(`${this.UrlTi}`);
  }
  listarprodutividade(){
    return this.http.get<any[]>(`${this.UrlProdutividade}`);
  }
  listarpessoal(){
    return this.http.get<any[]>(`${this.UrlPessoal}`);
  }
  listarfotografia(){
    return this.http.get<any[]>(`${this.UrlFotografia}`);
  }
  listarensino(){
    return this.http.get<any[]>(`${this.UrlEnsino}`);
  }

  


}
