import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { CursoService } from '../../curso.service';


@Component({
  selector: 'app-saude',
  templateUrl: './saude.component.html',
  styleUrls: ['./saude.component.scss']
})
export class SaudeComponent implements OnInit {

  cursos: any[] = [];
  form: FormGroup;

  constructor(private cursoService: CursoService, private fb: FormBuilder) { }

   ngOnInit () {
    this.listarsaude();
    this.form = this.fb.group({
      search: '', selecionado: ''
    })
   }

  listarsaude(){
    this.cursoService.listarsaude().subscribe(dados => this.cursos = dados);
   }


   get filtredCurso() {
     const result = this.cursos.filter(
       curso => curso.titulo.toLowerCase().includes(
         this.form.get('search').value.toLowerCase()
       ));
     return result;
   }
 
 
 
   listar() {
     this.cursoService.listarsaude().subscribe(res => {
       this.cursos = res;
     }, err => {
       console.log('err', err);
     });
   }
   listarAZ() {
     this.cursoService.listarsaude().subscribe(res => {
       this.cursos = res.sort((n1, n2) => {
         if (n1.titulo > n2.titulo) {
           return 1;
         }
 
         if (n1.titulo < n2.titulo) {
           return -1;
         }
 
         return 0;
       });
     }, err => {
       console.log('err', err);
     });
   }
 
   listarZA() {
     this.cursoService.listarsaude().subscribe(res => {
       this.cursos = res.sort((n1, n2) => {
         if (n1.titulo > n2.titulo) {
           return -1;
         }
 
         if (n1.titulo < n2.titulo) {
           return 1;
         }
 
         return 0;
       });
     }, err => {
       console.log('err', err);
     });
   }
 
   listarAZprof() {
     this.cursoService.listarsaude().subscribe(res => {
       this.cursos = res.sort((n1, n2) => {
         if (n1.prof > n2.prof) {
           return 1;
         }
 
         if (n1.prof < n2.prof) {
           return -1;
         }
 
         return 0;
       });
     }, err => {
       console.log('err', err);
     });
   }
   listarZAprof() {
     this.cursoService.listarsaude().subscribe(res => {
       this.cursos = res.sort((n1, n2) => {
         if (n1.prof > n2.prof) {
           return -1;
         }
 
         if (n1.prof < n2.prof) {
           return 1;
         }
 
         return 0;
       });
     }, err => {
       console.log('err', err);
     });
   }
 
 
 
 }
 