import { Component, OnInit } from '@angular/core';
import { CursoService } from '../../curso.service';

@Component({
  selector: 'app-inicial',
  templateUrl: './inicial.component.html',
  styleUrls: ['./inicial.component.scss']
})
export class InicialComponent implements OnInit {

  cursosdesenvolvimento: Array<any>;
  cursosnegocios: Array<any>;
  cursosdesign: Array <any>;
  cursosfinancas: Array <any>;
  cursosestilo: Array <any>;
  cursosmarketing: Array <any>;
  cursosmusica: Array <any>;
  cursossaude: Array <any>;
  cursosti: Array <any>;
  cursosprodutividade: Array <any>;
  cursospessoal: Array <any>;
  cursosfotografia: Array <any>;
  cursosensino: Array <any>;


  constructor(private cursoService: CursoService) { }

  ngOnInit(): void {
    this.listardesenvolvimento();
    this.listarnegocio();
    this.listardesign();
    this.listarensino();
    this.listarestilo();
    this.listarfinancas();
    this.listarfotografia();
    this.listarmarketing();
    this.listarmusica();
    this.listarpessoal();
    this.listarsaude();
    this.listarti();
    this.listarprodutividade();

  }

  listardesenvolvimento(){
   this.cursoService.listardesenvolvimento().subscribe(dados => this.cursosdesenvolvimento = dados);
  }
  listarnegocio(){
    this.cursoService.listarnegocios().subscribe(dados => this.cursosnegocios = dados);
  }
  listardesign(){
    this.cursoService.listardesign().subscribe(dados => this.cursosdesign = dados);
  }
  listarfinancas(){
    this.cursoService.listarfinancas().subscribe(dados => this.cursosfinancas = dados);
  }
  listarestilo(){
    this.cursoService.listarestilo().subscribe(dados => this.cursosestilo = dados);
  }
  listarmarketing(){
    this.cursoService.listarmarketing().subscribe(dados => this.cursosmarketing = dados);
  }
  listarmusica(){
    this.cursoService.listarmusica().subscribe(dados => this.cursosmusica = dados);
  }
  listarsaude(){
    this.cursoService.listarsaude().subscribe(dados => this.cursossaude = dados);
  }
  listarti(){
    this.cursoService.listarti().subscribe(dados => this.cursosti = dados);
  }
  listarprodutividade(){
    this.cursoService.listarprodutividade().subscribe(dados => this.cursosprodutividade = dados);
  }
  listarpessoal(){
    this.cursoService.listarpessoal().subscribe(dados => this.cursospessoal = dados);
  }
  listarfotografia(){
    this.cursoService.listarfotografia().subscribe(dados => this.cursosfotografia = dados);
  }
  listarensino(){
    this.cursoService.listarensino().subscribe(dados => this.cursosensino = dados);
  }
  }

