import { Component, OnInit } from '@angular/core';
import { CursoService } from '../../curso.service';
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-design',
  templateUrl: './design.component.html',
  styleUrls: ['./design.component.scss']
})
export class DesignComponent implements OnInit {

  cursos: any[] = [];
  form: FormGroup;

  constructor(private cursoService: CursoService, private fb: FormBuilder) { }

  ngOnInit() {
    this.listardesign();
    this.form = this.fb.group({
      search: '', selecionado: ''
    })
  }

  listardesign() {
    this.cursoService.listardesign().subscribe(dados => this.cursos = dados);
  }


  get filtredCurso() {
    const result = this.cursos.filter(
      curso => curso.titulo.toLowerCase().includes(
        this.form.get('search').value.toLowerCase()
      ));
    return result;
  }


  listar() {
    this.cursoService.listardesign().subscribe(res => {
      this.cursos = res;
    }, err => {
      console.log('err', err);
    });
  }
  listarAZ() {
    this.cursoService.listardesign().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.titulo > n2.titulo) {
          return 1;
        }

        if (n1.titulo < n2.titulo) {
          return -1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }

  listarZA() {
    this.cursoService.listardesign().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.titulo > n2.titulo) {
          return -1;
        }

        if (n1.titulo < n2.titulo) {
          return 1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }

  listarAZprof() {
    this.cursoService.listardesign().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.prof > n2.prof) {
          return 1;
        }

        if (n1.prof < n2.prof) {
          return -1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }
  listarZAprof() {
    this.cursoService.listardesign().subscribe(res => {
      this.cursos = res.sort((n1, n2) => {
        if (n1.prof > n2.prof) {
          return -1;
        }

        if (n1.prof < n2.prof) {
          return 1;
        }

        return 0;
      });
    }, err => {
      console.log('err', err);
    });
  }
}
