import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursosListagemComponent } from './cursos-listagem/cursos-listagem.component';
import { NegociosComponent } from './pages/negocios/negocios.component';
import { DesignComponent } from './pages/design/design.component';
import { DesenvolvimentoComponent } from './pages/desenvolvimento/desenvolvimento.component';
import { FinancasComponent } from './pages/financas/financas.component';
import { SaudeComponent } from './pages/saude/saude.component';
import { TiComponent } from './pages/ti/ti.component';
import { EstiloComponent } from './pages/estilo/estilo.component';
import { MarketingComponent } from './pages/marketing/marketing.component';
import { MusicaComponent } from './pages/musica/musica.component';
import { ProdutividadeComponent } from './pages/produtividade/produtividade.component';
import { PessoalComponent } from './pages/pessoal/pessoal.component';
import { FotografiaComponent } from './pages/fotografia/fotografia.component';
import { EnsinoComponent } from './pages/ensino/ensino.component';
import { InicialComponent } from './pages/inicial/inicial.component';

const routes: Routes = [
  { path:'todos', component: CursosListagemComponent},
  { path:'negocios', component: NegociosComponent},
  { path:'design', component: DesignComponent},
  { path:'desenvolvimento', component: DesenvolvimentoComponent},
  { path:'financas', component: FinancasComponent},
  { path:'saude', component: SaudeComponent},
  { path:'ti', component: TiComponent},
  { path:'estilo', component: EstiloComponent},
  { path:'marketing', component: MarketingComponent},
  { path:'musica', component: MusicaComponent},
  { path:'produtividade', component: ProdutividadeComponent},
  { path:'pessoal', component: PessoalComponent},
  { path:'fotografia', component: FotografiaComponent},
  { path:'ensino', component: EnsinoComponent},
  { path:'', component: InicialComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
