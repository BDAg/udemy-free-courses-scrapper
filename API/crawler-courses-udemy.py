import requests
import csv
from pymongo import MongoClient

# DB connectivity
client = MongoClient('localhost', 27017)
db = client.freeudemy
collection_cursos = db.cursos

c = csv.writer(open('cursos.csv', 'w'))
c.writerow(['ID', 'Título', 'Descrição', 'Categoria',
            'Link', 'URL_IMG', 'Professor', 'URL_PROF'])

cabecalho = {
    "Accept": "application/json, text/plain, /",
    "Authorization": "Basic S3dGem5iT20zYW9ON0lDcFdHMHpoa2lNa2R5azJIZExmTnlIUnFiVzp0cFdnbWtOWWlSTnJITGNFbTluWDVkQzhiVHJDbmRueEtyamdoTWdjS3BhMUlIMjhKcTVVNndxY3JFdWc2MXlQR3pEMVYxSFlseVJiNGw3U1ZIbXdDc1d6Y3BFeXJFSzBESEJqeUlQRDVDUlRWNTBnN1M1VVNOZnlqZG1NMmZRSg==",
    "Content-Type": "application/json;charset=utf-8"
}

categorias = ['Business', 'Design', 'Development', 'Finance+%26+Accounting', 'Health+%26+Fitness', 'IT+%26+Software',
              'Lifestyle', 'Marketing', 'Music', 'Office%20Productivity', 'Personal%20Development', 'Photography', 'Teaching+%26+Academics']

collection_cursos.drop({})

for x in range(len(categorias)):
    proximapag = 'proximapag'
    url = 'https://www.udemy.com/api-2.0/courses/?page_size=100&category=' + \
        categorias[x]+'&price=price-free&language=pt&ordering=highest-rated'

    while proximapag != None:
        cursos = requests.get(url, headers=cabecalho)

        for i in range(len(cursos.json()['results'])):
            id = cursos.json()['results'][i]['id']
        # print(id)
            titulo = cursos.json()['results'][i]['title']
        # print(titulo)
            descricao = cursos.json()['results'][i]['headline']
        # print(descricao)

            if categorias[x] == 'Business':
                categoria = 'Negócios'
            elif categorias[x] == 'Design':
                categoria = 'Design'
            elif categorias[x] == 'Development':
                categoria = 'Desenvolvimento'
            elif categorias[x] == 'Finance+%26+Accounting':
                categoria = 'Finanças e Contabilidade'
            elif categorias[x] == 'Lifestyle':
                categoria = 'Estilo de Vida'
            elif categorias[x] == 'Marketing':
                categoria = 'Marketing'
            elif categorias[x] == 'Music':
                categoria = 'Música'
            elif categorias[x] == 'Health+%26+Fitness':
                categoria = 'Saúde e Fitness'
            elif categorias[x] == 'IT+%26+Software':
                categoria = 'TI e Software'
            elif categorias[x] == 'Office%20Productivity':
                categoria = 'Produtividade no Escritório'
            elif categorias[x] == 'Personal%20Development':
                categoria = 'Desenvolvimento Pessoal'
            elif categorias[x] == 'Photography':
                categoria = 'Fotografia'
            elif categorias[x] == 'Teaching+%26+Academics':
                categoria = 'Ensino e Estudo Acadêmico'

            # print(categorias[x])
            # print([id, titulo, descricao, categoria, link, link_imagem, prof, link_prof])
            link = "https://www.udemy.com"+cursos.json()['results'][i]['url']
            # print(link)
            link_imagem = cursos.json()['results'][i]['image_480x270']
            # print(link_imagem)
            prof = (cursos.json()['results'][i]
                    ['visible_instructors'][0]['display_name'])
            # print(prof)
            link_prof = ("https://www.udemy.com"+cursos.json()
                         ['results'][i]['visible_instructors'][0]['url'])
            # print(link_prof)
            # print(' ')
            c.writerow([id, titulo, descricao, categoria,
                        link, link_imagem, prof, link_prof])

            # Final insert statement

            collection_cursos.insert({
                'id': id,
                'titulo': titulo,
                'descricao': descricao,
                'categoria': categoria,
                'link': link,
                'link_imagem': link_imagem,
                'prof': prof,
                'link_prof': link_prof
            })

            print(titulo)

            proximapag = cursos.json()['next']
            # print(proximapag)
            if proximapag != 'None':
                url = proximapag