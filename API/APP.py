# -- coding: utf-8 --
#!/usr/bin/python3

from flask_cors import CORS, cross_origin
from flask_pymongo import PyMongo
from flask import Flask, jsonify, request, escape
from crawler import CrawlerUdemy

app = Flask(__name__)
cors = CORS(app)

app.config['MONGO_DBNAME'] = 'freeudemy'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/freeudemy'

mongo = PyMongo(app)

@app.route('/', methods=['GET'])
@cross_origin()
def geral__():
    freeudemy = mongo.db.cursos
    udemy = []
    curso = freeudemy.find()
    for j in curso:
        j.pop('_id')
        udemy.append(j)
    return jsonify(udemy)


@app.route('/negocios', methods=['GET'])
@cross_origin()
def negocios__():
    freeudemy = mongo.db.cursos
    neg = []
    negocios = freeudemy.find({"categoria": "Negócios"})
    for j in negocios:
        j.pop('_id')
        neg.append(j)
    return jsonify(neg)


@app.route('/design', methods=['GET'])
def design__():
    freeudemy = mongo.db.cursos
    des = []
    design = freeudemy.find({"categoria": "Design"})
    for j in design:
        j.pop('_id')
        des.append(j)
    return jsonify(des)


@app.route('/desenvolvimento', methods=['GET'])
def desenvolvimento__():
    freeudemy = mongo.db.cursos
    desenv = []
    desenvolvimento = freeudemy.find({"categoria": "Desenvolvimento"})
    for j in desenvolvimento:
        j.pop('_id')
        desenv.append(j)
    return jsonify(desenv)


@app.route('/financas', methods=['GET'])
def financas__():
    freeudemy = mongo.db.cursos
    finan = []
    financas = freeudemy.find({"categoria": "Finanças e Contabilidade"})
    for j in financas:
        j.pop('_id')
        finan.append(j)
    return jsonify(finan)


@app.route('/estilo', methods=['GET'])
def estilo__():
    freeudemy = mongo.db.cursos
    esti = []
    estilo = freeudemy.find({"categoria": "Estilo de Vida"})
    for j in estilo:
        j.pop('_id')
        esti.append(j)
    return jsonify(esti)


@app.route('/marketing', methods=['GET'])
def marketing__():
    freeudemy = mongo.db.cursos
    mark = []
    marketing = freeudemy.find({"categoria": "Marketing"})
    for j in marketing:
        j.pop('_id')
        mark.append(j)
    return jsonify(mark)


@app.route('/musica', methods=['GET'])
def musica__():
    freeudemy = mongo.db.cursos
    mus = []
    musica = freeudemy.find({"categoria": "Música"})
    for j in musica:
        j.pop('_id')
        mus.append(j)
    return jsonify(mus)


@app.route('/saude', methods=['GET'])
def saude__():
    freeudemy = mongo.db.cursos
    sau = []
    saude = freeudemy.find({"categoria": "Saúde e Fitness"})
    for j in saude:
        j.pop('_id')
        sau.append(j)
    return jsonify(sau)


@app.route('/ti', methods=['GET'])
def ti__():
    freeudemy = mongo.db.cursos
    ti = []
    sof = freeudemy.find({"categoria": "TI e Software"})
    for j in sof:
        j.pop('_id')
        ti.append(j)
    return jsonify(ti)


@app.route('/produtividade', methods=['GET'])
def produtividade__():
    freeudemy = mongo.db.cursos
    prod = []
    produtividade = freeudemy.find({"categoria": "Produtividade no Escritório"})
    for j in produtividade:
        j.pop('_id')
        prod.append(j)
    return jsonify(prod)


@app.route('/pessoal', methods=['GET'])
def pessoal__():
    freeudemy = mongo.db.cursos
    pess = []
    pessoal = freeudemy.find({"categoria": "Desenvolvimento Pessoal"})
    for j in pessoal:
        j.pop('_id')
        pess.append(j)
    return jsonify(pess)


@app.route('/fotografia', methods=['GET'])
def fotografia__():
    freeudemy = mongo.db.cursos
    foto = []
    fotografia = freeudemy.find({"categoria": "Fotografia"})
    for j in fotografia:
        j.pop('_id')
        foto.append(j)
    return jsonify(foto)


@app.route('/ensino', methods=['GET'])
def ensino__():
    freeudemy = mongo.db.cursos
    ens = []
    ensino = freeudemy.find({"categoria": "Ensino e Estudo Acadêmico"})
    for j in ensino:
        j.pop('_id')
        ens.append(j)
    return jsonify(ens)


app.run('0.0.0.0', 3000, debug=True)
