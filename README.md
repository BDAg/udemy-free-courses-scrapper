# Free Courses


## 📋 Pré Requisitos: 
Python 3.8 | MongoDB | NPM


## ⚙️ Instalação
- Angular

`npm install -g @angular/cli`

- Python Requests

`pip install requests`

- Python Pymongo

`python -m pip install pymongo`

- Python Flask

`pip install Flask`

`pip install Flask-PyMongo`

`pip install Flask-Cors`



## 📥 Donwload do Projeto 

`git clone https://gitlab.com/BDAg/udemy-free-courses-scrapper/`

## ✅ Execução do Projeto

Entrar na pasta **API/**

Primeiro passo é rodar o bot

`python crawler-courses-udemy.py`

O algoritmo vai fazer uma requisição para API da Udemy, retornando todos os cursos grátis e inserir no Banco *freeudemy*.

Após rodar o crawler, inicializar API

`python APP.py`


Entrar na pasta **front/**

Dar um comando para atualizar dependências do Angular

`npm install `

Após carregar, rodar

`ng serve`


Abrir navegador localhost:4200/




